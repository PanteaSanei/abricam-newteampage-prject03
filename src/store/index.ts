import Vue from 'vue'
import Vuex from 'vuex'
import {TeamsModule} from './modules/teams'
// Load Vuex
Vue.use(Vuex)
//Create Store
export default new Vuex.Store({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    teams : TeamsModule
  }
})
