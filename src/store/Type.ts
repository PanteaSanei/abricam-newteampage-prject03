
// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace TeamFormat {

    export interface Team {
        id : number,
        name : string,
        description : string | null,
        resourceType : string,
        key: number,
        createdAt : string,
    }

    export interface TeamState {
        teams ?: Array<Team>;
    }
}