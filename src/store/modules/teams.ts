import {Module} from "vuex";
import {TeamFormat} from "@/store/Type";
import axios, {AxiosResponse} from "axios";

export const state: TeamFormat.TeamState = {
    teams: [],
}

export const TeamsModule: Module<TeamFormat.TeamState, never> = {
    namespaced: true,
    state,
    actions : {
        getTeams({commit}){
            axios.get('http://localhost:3000/teams')
                 .then((responseData : AxiosResponse) => {
                     commit('append' , responseData.data)
            })
        }
    },
    mutations : {
        append(state, responseData){
            state.teams = responseData
        }
    },
    getters : {
        teams: state => state.teams,
    },
}